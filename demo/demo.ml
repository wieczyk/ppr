open Ppr.PlainTextRenderer
open Ppr.Combinators

let test = pp_group
    [ pp_text "before"
    ; pp_syntax_nosp_after "("
    ; pp_keyword "nawias"
    ; pp_identifier "inside"
    ; pp_syntax_nosp_before ")"
    ; pp_text "after"
    ; PPList.pp_map pp_value_int [1;2;3;4]
    ]

let _ = print_endline (Ppr.StdoutTextRenderer.render test)

