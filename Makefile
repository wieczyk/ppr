INCLUDES=-I source
OCAMLBUILD_FLAGS=-use-ocamlfind ${INCLUDES}
OCAMLFIND=ocamlfind
TARGET=byte
LIB_FILES=_build/Ppr.cmxa _build/Ppr.cma _build/source/Ppr.cmi _build/Ppr.a
.PHONY: all clean install remove deinstall uninstall 

all:
	ocamlbuild ${OCAMLBUILD_FLAGS} Ppr.cmxa Ppr.cma demo/demo.byte

clean:
	ocamlbuild ${OCAMLBUILD_FLAGS} Ppr.cmxa Ppr.cma demo/demo.byte -clean

install: all
	${OCAMLFIND} install ppr META ${LIB_FILES}

remove deinstall uninstall:
	${OCAMLFIND} remove ppr

