open Data
open BaseCombinators

type combinator
    = L_Word of string
    | L_PushCategory of category 
    | L_SkipSpace
    | L_PopCategory of category

(*
module Normalizer(M : sig end) = struct

    type output_element  = int * combinator list

    type state =
        { mutable categories    : CategorySet.t
        ; mutable output_stack  : output_element list
        ; mutable current_line  : combinator list
        ; mutable indent        : int
        }

    let state =
        { categories    = CategorySet.empty
        ; output_stack  = []
        ; current_line  = []
        ; indent        = 0
        }

    let append l = 
        state.current_line <- l :: state.current_line

    let push_category cat =
        let b = CategorySet.mem cat state.categories in
        if not b then
            state.categories <- CategorySet.add cat state.categories;
        not b

    let rec execute = function
        | (L_PushCategory cat) as c :: rest ->
            if push_category cat then
                append c;
            execute rest

        | l :: rest ->
            append l;
            execute rest

        | [] ->
            List.rev state.current_line

end
*)

module Compiler (M : sig end) = struct

    type task
        = Group of int * BaseCombinators.combinator list
        | PopCategory of category

    type output_element  = int * combinator list

    type state =
        { mutable indent            : int
        ; mutable task_stack        : task list
        ; mutable output_stack      : output_element list
        ; mutable current_line      : combinator list 
        }

    let state = 
        { indent        = 0
        ; task_stack    = []
        ; output_stack  = []
        ; current_line  = []
        }

    let append l = 
        state.current_line <- l :: state.current_line

    let commit_current_line () =
        let line = (state.indent, List.rev state.current_line) in
        state.output_stack <- line :: state.output_stack;
        state.current_line <- []

    let add_task_group grp =
        let task = Group (state.indent, grp) in
        state.task_stack <- task :: state.task_stack

    let add_task_pop_category cat =
        let task = PopCategory cat in
        state.task_stack <- task :: state.task_stack

    let rec execute = function
        | B_Word txt :: rest ->
            append (L_Word txt);
            execute rest

        | B_Indent i :: rest ->
            add_task_group rest;
            state.indent <- succ state.indent;
            execute [i; B_NewLine]

        | B_Group grp :: rest ->
            add_task_group rest;
            execute grp

        | B_Break :: rest ->
            if state.current_line != [] then begin
                commit_current_line ()
            end;
            execute rest

        | B_NewLine :: rest ->
            commit_current_line ();
            execute rest

        | B_SkipSpace :: rest ->
            append (L_SkipSpace);
            execute rest

        | B_MarkCategory (cat, c) :: rest ->
            add_task_group rest;
            add_task_pop_category cat;
            append (L_PushCategory cat);
            execute [c]

        | [] ->
            process_next_task_or_halt ()

    and process_next_task_or_halt () =
        match state.task_stack with
        | [] ->
            if state.current_line != [] then
                commit_current_line ()

        | PopCategory cat :: xs ->
            append (L_PopCategory cat);
            state.task_stack <- xs;
            process_next_task_or_halt ()

        | Group (i,x) :: xs ->
            state.indent <- i;
            state.task_stack <- xs;
            execute x

    let compile i = 
        execute [i];
        List.rev (state.output_stack)
end

let compile i = 
    let module M = Compiler(struct end) in
    M.compile i

