open RendererInstructions
open Data

module StringMap = Map.Make(struct
    type t = string
    let compare = compare
end)

type tty_color
    = Black
    | Red
    | Green
    | Yellow
    | Blue
    | Magenta
    | Cyan
    | White

type tty_attribute
    = Fg of tty_color
    | Bg of tty_color
    | Bold

type configuration =
    { category_map  : tty_attribute list StringMap.t }

let renderer_default_categories =
    [ ("keyword",       [Fg Blue; Bold])
    ; ("syntax",        [Fg Black; Bold])
    ; ("value",         [Fg Cyan])
    ; ("identifier",    [Fg Green])
    ; ("operator",      [Fg Yellow])
    ]

let default_configuration = 
    { category_map  =
        let rec loop m = function
            | [] ->
                m
            | (k, v) :: xs ->
                let m = StringMap.add k v m in
                loop m xs
        in
        loop StringMap.empty renderer_default_categories
    }

module type Specification = sig

    val space : string

    val before_text : string

    val after_text : string

    val before_category : configuration -> category -> string

    val after_category : configuration -> category -> string

    val indent : string

    val eol : string

    val last_eol : string
end

module Make (SPEC : Specification) = struct

    module type RuntimeConfiguration = sig
        val configuration : configuration
    end

    module Renderer (M: RuntimeConfiguration) = struct
        open M

        type state =
            { buffer    : Buffer.t
            }

        let state =
            { buffer    = Buffer.create 1024
            }


        let interpret_instruction = function
            | I_EmitSpace ->
                Buffer.add_string state.buffer SPEC.space;

            | I_EmitText txt ->
                Buffer.add_string state.buffer SPEC.before_text;
                Buffer.add_string state.buffer txt;
                Buffer.add_string state.buffer SPEC.after_text;

            | I_PushCategory cat ->
                Buffer.add_string state.buffer
                    (SPEC.before_category M.configuration cat)

            | I_PopCategory cat ->
                Buffer.add_string state.buffer
                    (SPEC.after_category M.configuration cat)

        let interpret_line (indent, instrs) =
            let rec loop = function
                | 0 ->
                    ()
                | k ->
                    Buffer.add_string state.buffer SPEC.indent;
                    loop (pred k)
            in
            loop indent;
            List.iter interpret_instruction instrs


        let rec interpret_lines = function
            | [] ->
                Buffer.add_string state.buffer SPEC.last_eol
            | [x] ->
                interpret_line x;
                Buffer.add_string state.buffer SPEC.last_eol
            | x::xs ->
                interpret_line x;
                Buffer.add_string state.buffer SPEC.eol;
                interpret_lines xs

        let interpret lines =
            interpret_lines lines;
            Buffer.contents state.buffer

    end

    let interpret ?(configuration=default_configuration) lines =
        let module M = Renderer(struct let configuration = configuration end) in
        M.interpret lines

    let render ?(configuration=default_configuration) printer =
        printer
        |> BaseCombinators.simplify_printer 
        |> LineCombinators.compile
        |> RendererInstructions.compile
        |> interpret ~configuration:configuration

end
