open Data

type combinator
    = C_Text of string
    | C_Indent of printer
    | C_Group of printer list
    | C_MarkCategory of category * printer
    | C_Break
    | C_NewLine
    | C_SkipSpace

and printer = 
    { printer_priority   : int
    ; printer_combinator : combinator
    }

let embed p =
    { printer_priority   = 0
    ; printer_combinator = p
    }

let priority_of_printer pp = pp.printer_priority

let rec is_pp_singleline pp =
    match pp.printer_combinator with
    | C_Break -> false
    | C_NewLine -> false
    | C_Group ls -> List.for_all is_pp_singleline ls
    | C_MarkCategory (_, c) -> is_pp_singleline c
    | C_Indent c -> is_pp_singleline c
    | C_Text _ -> true
    | C_SkipSpace -> true

let pp_group xs =
    { printer_priority = 0
    ; printer_combinator = C_Group xs
    }

let pp_empty = pp_group []

let pp_indent x =
    embed (C_Indent x)

let pp_indent_when_ml x =
    if is_pp_singleline x then
        x
    else
        pp_indent x

let pp_indent_group xs =
    pp_indent (pp_group xs)

let pp_indent_group_when_ml x =
    let x = pp_group x in
    pp_indent_when_ml x

let pp_text txt =
    { printer_priority = 0
    ; printer_combinator = C_Text txt
    }

let pp_int i =
    pp_text (string_of_int i)

let pp_int32 i32 =
    pp_text (Int32.to_string i32)

let pp_int64 i64 =
    pp_text (Int64.to_string i64)

let pp_break =
    embed (C_Break)

let pp_newline =
    embed (C_NewLine)

let pp_skipspace =
    embed (C_SkipSpace)

let pp_category cat  c =
    embed (C_MarkCategory (Category cat, c))

let pp_text_nosp_before txt =
    pp_group [ pp_skipspace; pp_text txt ]

let pp_text_nosp_after txt =
    pp_group [ pp_text txt; pp_skipspace ]

let pp_brackets pp_left pp_right pp =
    pp_group [pp_left; pp; pp_right]

let pp_prefix prefix pp =
    pp_group [prefix; pp]

let pp_suffix suffix pp =
    pp_group [pp; suffix]

let pp_keyword txt =
    pp_category "keyword" (pp_text txt)

let pp_syntax_nosp_before txt =
    pp_category "syntax" (pp_text_nosp_before txt)

let pp_syntax_nosp_after txt =
    pp_category "syntax" (pp_text_nosp_after txt)

let pp_left_bracket = pp_syntax_nosp_after "("
let pp_right_bracket = pp_syntax_nosp_before ")"

let pp_left_sqbracket = pp_syntax_nosp_after "["
let pp_right_sqbracket = pp_syntax_nosp_before "]"

let pp_left_curlybracket = pp_syntax_nosp_after "{"
let pp_right_curlybracket = pp_syntax_nosp_before "}"

let pp_sep_coma = pp_syntax_nosp_before ","
let pp_sep_semicolon = pp_syntax_nosp_before ";"

let pp_value txt =
    pp_category "value" (pp_text txt)

let pp_value_int i =
    pp_category "value" (pp_int i)

let pp_value_int32 i =
    pp_category "value" (pp_int32 i)

let pp_value_int64 i =
    pp_category "value" (pp_int64 i)

let pp_identifier txt =
    pp_category "identifier" (pp_text txt)

let pp_operator txt =
    pp_category "operator" (pp_text txt)

let pp_manysep pp_sep = function
    | [] ->
        pp_empty
    | [pp] ->
        pp
    | pp :: pps ->
        let pp_xs = List.map (pp_prefix pp_sep) pps in
        pp_group
            [ pp
            ; pp_group pp_xs
            ]

let pp_manysep_map pp_sep f xs = 
    pp_manysep pp_sep (List.map f xs)

module PPArray = struct

    let pp_manysep_map pp_sep f arr =
        pp_manysep_map pp_sep f (Array.to_list arr)

    let pp_map
            ?(left=pp_left_sqbracket)
            ?(right=pp_right_sqbracket)
            ?(sep=pp_sep_semicolon)
            f arr =
        pp_group
            [ left
            ; pp_manysep_map sep f arr
            ; right
            ]

end

module PPList = struct

    let pp_list
            ?(left=pp_left_sqbracket)
            ?(right=pp_right_sqbracket)
            ?(sep=pp_sep_semicolon)
            pps =
        pp_group
            [ left
            ; pp_manysep sep pps
            ; right
            ]

    let pp_map
            ?(left=pp_left_sqbracket)
            ?(right=pp_right_sqbracket)
            ?(sep=pp_sep_semicolon)
            f xs =
        pp_list
            ~left:left
            ~right:right
            ~sep:sep
            (List.map f xs)

end

module PPSet (M:Set.S) = struct

    let pp_manysep_map pp_sep f s =
        pp_manysep_map pp_sep f (M.elements s)

    let pp_map
            ?(left=pp_left_curlybracket)
            ?(right=pp_right_curlybracket)
            ?(sep=pp_sep_semicolon)
            f s =
        pp_group
            [ left
            ; pp_manysep_map sep f s
            ; right
            ]
end

module PPMap (M:Map.S) = struct

    let pp_manysep_map pp_sep f s =
        pp_manysep_map pp_sep f (M.bindings s)

    let pp_map
            ?(left=pp_left_curlybracket)
            ?(right=pp_right_curlybracket)
            ?(sep=pp_sep_semicolon)
            f s =
        pp_group
            [ left
            ; pp_manysep_map sep f s
            ; right
            ]
end

