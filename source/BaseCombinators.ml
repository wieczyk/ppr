open Data
open Combinators

type combinator
    = B_Word of string
    | B_Indent of combinator
    | B_Group of combinator list
    | B_MarkCategory of category * combinator
    | B_Break
    | B_NewLine
    | B_SkipSpace


let rec simplify_printer = function
    | { printer_combinator = c; printer_priority = _ } ->
        simplify_combinator c

and simplify_combinator = function
    | C_Text str ->
        B_Word str

    | C_SkipSpace ->
        B_SkipSpace

    | C_Indent p ->
        B_Group [B_Break; B_Indent (simplify_printer p)]

    | C_Group ps ->
        B_Group (List.map simplify_printer ps)

    | C_MarkCategory (c, p) ->
        B_MarkCategory (c, simplify_printer p)

    | C_Break ->
        B_Break

    | C_NewLine ->
        B_NewLine

