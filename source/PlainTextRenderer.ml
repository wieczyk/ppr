open GenericTextRenderer


module Implementation = Make(struct
    let space = " "
    let indent = "    "
    let eol = "\n"
    let last_eol = ""

    let before_text = ""
    let after_text = ""

    let before_category _ _ = ""
    let after_category _ _ = ""
end)

include Implementation
