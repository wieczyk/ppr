open Data
open LineCombinators

type instruction
    = I_EmitSpace
    | I_EmitText of string
    | I_PushCategory of category
    | I_PopCategory of category

(*
    | I_PushAttribute of attribute list
    | I_PopAttribute of attribute list
*)

module Compiler (M: sig end) = struct

    type output_element = int * instruction list

    type task = int * LineCombinators.combinator list

    type state =
        { mutable indent        : int
        ; mutable task_stack    : task list
        ; mutable output_stack  : output_element list
        ; mutable current_line  : instruction list
        ; mutable need_space    : bool
        ; mutable attrs         : int AttributeMap.t
        }

    let state =
        { indent        = 0
        ; task_stack    = []
        ; output_stack  = []
        ; current_line  = []
        ; need_space    = false
        ; attrs         = AttributeMap.empty
        }

    let append l = 
        state.current_line <- l :: state.current_line

    let push_attributes attrs =
        let h attr = 
            try
                let i = AttributeMap.find attr state.attrs in
                state.attrs <- AttributeMap.add attr (succ i) state.attrs
            with Not_found ->
                state.attrs <- AttributeMap.add attr 0 state.attrs
        in
        List.iter h attrs

    let pop_attributes attrs =
        let h attr = 
            try
                let i = AttributeMap.find attr state.attrs in
                if i > 1 then
                    state.attrs <- AttributeMap.add attr (pred i) state.attrs
                else
                    state.attrs <- AttributeMap.remove attr state.attrs
            with Not_found ->
                Printf.eprintf "RendererInstructions bad pop attributes\n"
        in
        List.iter h attrs

    let test_attribute attr =
        AttributeMap.mem attr state.attrs

    let emit_space_before_when_needed () =
        if state.need_space && not (test_attribute NoSpaceBefore) then begin
            append (I_EmitSpace)
        end

    let set_space_is_needed () =
        state.need_space <- not (test_attribute NoSpaceAfter)

    let commit_current_line () =
        let p = (state.indent, List.rev state.current_line) in
        state.current_line <- [];
        state.output_stack <- p :: state.output_stack
        

    let rec execute = function
        | L_Word x :: rest ->
            emit_space_before_when_needed ();
            append (I_EmitText x);
            set_space_is_needed ();
            execute rest

        | L_SkipSpace :: rest ->
            state.need_space <- false;
            execute rest

        | L_PushCategory cat :: rest ->
            append (I_PushCategory cat);
            execute rest

        | L_PopCategory cat :: rest ->
            append (I_PopCategory cat);
            execute rest
(*
        | L_PushAttributes attrs :: rest ->
            push_attributes attrs;
            execute rest

        | L_PopAttributes attrs :: rest ->
            pop_attributes attrs;
            execute rest
*)
        | [] ->
            commit_current_line ();
            process_next_task_or_halt ()

    and process_next_task_or_halt () =
        match state.task_stack with
        | [] ->
            List.rev state.output_stack

        | (i, c) :: xs ->
            state.task_stack <- xs;
            state.indent <- i;
            state.need_space <- false;
            execute c

    let start i =
        state.task_stack <- i;
        process_next_task_or_halt ()

end

let compile i =
    let module M = Compiler(struct end) in
    M.start i
