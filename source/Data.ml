type category
    = Category of string

type attribute
    = NoSpaceAfter
    | NoSpaceBefore

module CategorySet = Set.Make(struct
    type t = category
    let compare = compare 
end)


module AttributeSet = Set.Make(struct
    type t = attribute
    let compare = compare 
end)

module AttributeMap = Map.Make(struct
    type t = attribute
    let compare = compare 
end)

