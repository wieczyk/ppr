open GenericTextRenderer

let code_of_color = function
    | Black -> 0
    | Red -> 1
    | Green -> 2
    | Yellow -> 3
    | Blue -> 4
    | Magenta -> 5
    | Cyan -> 6
    | White -> 7
    

let code_of_attribute = function
    | Fg c -> 30 + code_of_color c
    | Bg c -> 40 + code_of_color c
    | Bold -> 1

let escape_sequence_of_attributes attrs =
    let codes = List.map code_of_attribute attrs in
    let b = Buffer.create 13 in
    let rec loop = function
        | [] ->
            ()

        | [attr] ->
            Buffer.add_string b (string_of_int attr);
            Buffer.add_string b "m"

        | attr :: attrs ->
            Buffer.add_string b (string_of_int attr);
            Buffer.add_string b ";";
            loop attrs
    in
        if codes = [] then
            ""
        else begin
            Buffer.add_string b "\x1b[";
            loop codes;
            Buffer.contents b
        end

module Implementation = Make(struct
    let space = " "
    let indent = "    "
    let eol = "\n"
    let last_eol = ""

    let before_text = ""
    let after_text = ""

    let before_category cfg (Data.Category x) =
        try
            let attrs = StringMap.find x cfg.category_map in
            escape_sequence_of_attributes attrs
        with Not_found ->
            ""

    let after_category cfg (Data.Category x) =
        try
            let attrs = StringMap.find x cfg.category_map in
            if attrs = [] then
                ""
            else
                "\x1b[0m"
        with Not_found ->
            ""

end)

include Implementation
