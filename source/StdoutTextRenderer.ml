let can_use_colors_for fileno =
    try
        Unix.isatty fileno && Sys.getenv "TERM" <> "dump"
    with Not_found ->
        false

module type T = module type of PlainTextRenderer

let select_module_for_fileno fileno =
    if can_use_colors_for fileno then
        (module TtyTextRenderer : T) 
    else
        (module PlainTextRenderer : T) 

module StdoutRenderer = (val (select_module_for_fileno Unix.stdout) : T)

include StdoutRenderer
